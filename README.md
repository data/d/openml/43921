# OpenML dataset: car_evaluation

https://www.openml.org/d/43921

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Car Evaluation Database was derived from a simple hierarchical decision model originally developed for the demonstration of DEX, M. Bohanec, V. Rajkovic: Expert system for decision making. Sistemica 1(1), pp. 145-157, 1990.). In this version duplicate rows are dropped

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43921) of an [OpenML dataset](https://www.openml.org/d/43921). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43921/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43921/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43921/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

